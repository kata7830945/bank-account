package fr.exaltit.medkhalilbankaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("fr.exaltit.medkhalilbankaccount.adapters.persistense.entities")
public class MedKhalilBankAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedKhalilBankAccountApplication.class, args);
    }

}
