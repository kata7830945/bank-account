package fr.exaltit.medkhalilbankaccount.adapters.persistense.entities;

import fr.exaltit.medkhalilbankaccount.domains.transaction.enums.TransactionType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transaction")
public class TransactionEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    private Double amount;

    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime timestamp;

    @PrePersist
    protected void onCreate() {
        timestamp = LocalDateTime.now();
    }

}

