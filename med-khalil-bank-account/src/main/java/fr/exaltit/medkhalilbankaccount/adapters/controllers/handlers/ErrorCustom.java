package fr.exaltit.medkhalilbankaccount.adapters.controllers.handlers;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
public class ErrorCustom {

    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private String messageDebug;

    private ErrorCustom() {
        timestamp = LocalDateTime.now();
    }

    public ErrorCustom(HttpStatus status) {
        this();
        this.status = status;
    }

    public ErrorCustom(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.message = "Oops.. Une erreur inconnue est survenue";
        this.messageDebug = ex.getLocalizedMessage();
    }

    public ErrorCustom(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = status;
        this.message = message;
        this.messageDebug = ex.getLocalizedMessage();
    }
    
}
