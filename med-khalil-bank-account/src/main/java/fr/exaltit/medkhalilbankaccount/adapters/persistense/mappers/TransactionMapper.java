package fr.exaltit.medkhalilbankaccount.adapters.persistense.mappers;


import fr.exaltit.medkhalilbankaccount.adapters.persistense.entities.TransactionEntity;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {
    public Transaction toTransactionDTO(TransactionEntity transactionEntity)  {
        return new Transaction(transactionEntity.getId(),
                                        transactionEntity.getType(),
                                        transactionEntity.getAmount(),
                                        transactionEntity.getTimestamp());
    }

    public TransactionEntity toTransactionEntity (Transaction transaction)  {
        return new TransactionEntity(transaction.getId(),
                                        transaction.getType(),
                                        transaction.getAmount(),
                                        transaction.getTimestamp());
        
    }

}
