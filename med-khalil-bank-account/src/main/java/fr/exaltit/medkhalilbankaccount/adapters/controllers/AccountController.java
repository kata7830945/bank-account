package fr.exaltit.medkhalilbankaccount.adapters.controllers;


import fr.exaltit.medkhalilbankaccount.application.usecases.ConsultAccountUseCase;
import fr.exaltit.medkhalilbankaccount.application.usecases.CreateAccountUseCase;
import fr.exaltit.medkhalilbankaccount.application.usecases.ModifyAccountUseCase;
import fr.exaltit.medkhalilbankaccount.domains.account.Account;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/accounts")
public class AccountController {

    @Autowired
    private CreateAccountUseCase createAccountUseCase;
    @Autowired
    private ConsultAccountUseCase consultAccountUseCase;
    @Autowired
    private ModifyAccountUseCase modifyAccountUseCase;

    @GetMapping
    public ResponseEntity<List<Account>> getAllAccounts() {
        List<Account> comptesBancairesList = consultAccountUseCase.consultAccount();
        return ResponseEntity.ok().body(comptesBancairesList);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable final String id) {
        Account account = consultAccountUseCase.consultAccount(UUID.fromString(id));
        return ResponseEntity.ok().body(account);
    }

    @PostMapping
    public ResponseEntity<Account> createAccount() {
        Account account = createAccountUseCase.createAccount();
        return ResponseEntity.status(HttpStatus.CREATED).body(account);
    }

    @GetMapping(value = "/{id}/balance")
    public ResponseEntity<Double> getBalance(@PathVariable final String id) {
        Double balance = consultAccountUseCase.consultBalance(UUID.fromString(id));
        return ResponseEntity.ok().body(balance);
    }

    @GetMapping(value = "/{id}/transactions")
    public ResponseEntity<List<Transaction>> getTransactions(@PathVariable final String id) {
        List<Transaction> transactions = consultAccountUseCase.consultTransactions(UUID.fromString(id));
        return ResponseEntity.ok().body(transactions);
    }

    @PutMapping(value = "/{id}/credit/{amount}")
    public HttpStatus creditAccount(@PathVariable final UUID id, @PathVariable final Double amount) {
        return modifyAccountUseCase.depositAmount(id, amount)
                                ? HttpStatus.ACCEPTED
                                : HttpStatus.FORBIDDEN;
    }

    @PutMapping(value = "/{id}/debit/{amount}")
    public HttpStatus debitAccount(@PathVariable final UUID id, @PathVariable final Double amount) {
        return modifyAccountUseCase.withdrawAmount(id, amount)
                                ? HttpStatus.ACCEPTED 
                                : HttpStatus.FORBIDDEN;
    }

    @PutMapping(value = "/{id}/close")
    public HttpStatus closeAccount(@PathVariable final String id) {
        return modifyAccountUseCase.closeAccount(UUID.fromString(id))
                                ? HttpStatus.ACCEPTED 
                                : HttpStatus.NOT_MODIFIED;
    }

    @PutMapping(value = "/{id}/activate")
    public HttpStatus activateAccount(@PathVariable final String id) {
        return modifyAccountUseCase.activate(UUID.fromString(id))
                                ? HttpStatus.ACCEPTED 
                                : HttpStatus.NOT_MODIFIED;
    }

    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteAccount(@PathVariable final String id) {
        modifyAccountUseCase.deleteAccount(UUID.fromString(id));
        return HttpStatus.NO_CONTENT;
    }

}
