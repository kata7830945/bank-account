package fr.exaltit.medkhalilbankaccount.adapters.persistense.mappers;

import fr.exaltit.medkhalilbankaccount.adapters.persistense.entities.AccountEntity;
import fr.exaltit.medkhalilbankaccount.domains.account.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class AccountMapper {

    @Autowired
    TransactionMapper transactionMapper;

    public Account toAccountDTO(AccountEntity accountEntity)  {
        return new Account(accountEntity.getId(),
                                    accountEntity.getBalance(),
                                    accountEntity.getStatus(),
                                    accountEntity.getTransactions()
                                                        .stream()
                                                        .map(transactionEntity -> transactionMapper.toTransactionDTO(transactionEntity))
                                                        .collect(Collectors.toList()));
        
    }

    public AccountEntity toAccountEntity(Account account)  {
        return new AccountEntity(account.getId(),
                                            account.getBalance(),
                                            account.getStatus(),
                                            account.getTransactions()
                                                        .stream()
                                                        .map(transaction -> transactionMapper.toTransactionEntity(transaction))
                                                        .collect(Collectors.toSet()));
        
    }

}
