package fr.exaltit.medkhalilbankaccount.adapters.controllers.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

/*
 * Handler Custom pour les exceptions susceptibles de survenir dans le programme.
 * Classe à enrichir au fur et à mesure avec autant de handlers spécifiques que d'exceptions attendues,
 * pour une gestion d'erreur propre et personnalisée. 
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    public static final String NO_ACCOUNT_FOUND = "No account found !";
    
        /**
         * Handler pour {@link NoSuchElementException}
         *
         * @param ex l'exception NoSuchElementException
         * @return {@link ErrorCustom}
         */
        @ExceptionHandler(NoSuchElementException.class)
        protected ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException ex) {
            ErrorCustom error = new ErrorCustom(HttpStatus.NOT_FOUND);
            error.setMessage(NO_ACCOUNT_FOUND);
            return buildResponseEntity(error);
        }

        /**
         * Handler pour {@link InternalServerError}
         *
         * @param ex l'exception {@link InternalServerError}
         * @return {@link ErrorCustom}
         */
        @ExceptionHandler(InternalServerError.class)
        protected ResponseEntity<Object> handleInternalServerError(InternalServerError ex) {
            ErrorCustom error = new ErrorCustom(HttpStatus.INTERNAL_SERVER_ERROR, ex);
            return buildResponseEntity(error);
        }
    
        private ResponseEntity<Object> buildResponseEntity(ErrorCustom error) {
            return new ResponseEntity<>(error, error.getStatus());
        } 
}
