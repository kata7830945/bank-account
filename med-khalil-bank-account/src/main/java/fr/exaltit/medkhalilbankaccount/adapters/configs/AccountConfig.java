package fr.exaltit.medkhalilbankaccount.adapters.configs;

import fr.exaltit.medkhalilbankaccount.adapters.persistense.repositories.AccountRepository;
import fr.exaltit.medkhalilbankaccount.application.usecases.account.AccountUseCase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccountConfig {

    @Bean
    AccountUseCase accountService(AccountRepository repository) {
        return new AccountUseCase(repository, repository, repository);
    }
}