package fr.exaltit.medkhalilbankaccount.adapters.persistense.repositories;

import fr.exaltit.medkhalilbankaccount.adapters.persistense.entities.AccountEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

@Transactional
public interface IAccountRepository extends JpaRepository<AccountEntity, Long> {
    Optional<AccountEntity> findById(UUID id);
} 
    