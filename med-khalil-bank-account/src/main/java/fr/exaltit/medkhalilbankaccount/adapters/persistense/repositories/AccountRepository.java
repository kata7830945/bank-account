package fr.exaltit.medkhalilbankaccount.adapters.persistense.repositories;

import fr.exaltit.medkhalilbankaccount.adapters.persistense.entities.AccountEntity;
import fr.exaltit.medkhalilbankaccount.adapters.persistense.mappers.AccountMapper;
import fr.exaltit.medkhalilbankaccount.application.ports.DeleteAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.ports.LoadAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.ports.SaveAccountProvider;
import fr.exaltit.medkhalilbankaccount.domains.account.Account;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class AccountRepository implements LoadAccountProvider, SaveAccountProvider, DeleteAccountProvider {

    private IAccountRepository repository;
    private AccountMapper accountMapper;

    @Override
    public List<Account> loadAll() {
        return repository.findAll()
                    .stream()
                    .map(compteBancaireEntity -> accountMapper.toAccountDTO(compteBancaireEntity))
                    .collect(Collectors.toList());
    }

    @Override
    public Optional<Account> loadAccount(UUID id) {
        Optional<AccountEntity> accountEntity = repository.findById(id);
        if (accountEntity.isPresent()) {
            return Optional.of(accountMapper.toAccountDTO(accountEntity.get()));
        } return Optional.empty();
    }

    @Override
    public void save(Account account) {
        repository.save(accountMapper.toAccountEntity(account));
    } 

    @Override
    public void delete(Account account) {
        repository.delete(accountMapper.toAccountEntity(account));
    }

}