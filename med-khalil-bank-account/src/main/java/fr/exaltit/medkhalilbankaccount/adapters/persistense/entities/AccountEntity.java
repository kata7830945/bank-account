package fr.exaltit.medkhalilbankaccount.adapters.persistense.entities;


import fr.exaltit.medkhalilbankaccount.domains.account.enums.AccountStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    private UUID id;

    private Double balance;

    @Enumerated(EnumType.STRING)
    private AccountStatus status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<TransactionEntity> transactions = new HashSet<>();

}
