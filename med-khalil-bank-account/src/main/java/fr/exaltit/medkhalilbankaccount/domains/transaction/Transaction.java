package fr.exaltit.medkhalilbankaccount.domains.transaction;

import fr.exaltit.medkhalilbankaccount.domains.transaction.enums.TransactionType;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    private UUID id;
    private TransactionType type;
    private Double amount;
    private LocalDateTime timestamp;
}
