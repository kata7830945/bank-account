package fr.exaltit.medkhalilbankaccount.domains.account;

import fr.exaltit.medkhalilbankaccount.domains.account.enums.AccountStatus;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;
import fr.exaltit.medkhalilbankaccount.domains.transaction.enums.TransactionType;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    public static final String WRONG_ARGUMENT_PASSED = "wrong argument is passed!";

    private UUID id;
    private Double balance;
    private AccountStatus status;
    private List<Transaction> transactions;

    public Account(UUID id, Double balance) {
        this(id);
        this.balance = balance;
    }

    public Account(UUID id, Double balance, List<Transaction> transactions) {
        this(id);
        this.balance = balance;
        this.transactions = transactions;
    }

    public Account(UUID id) {
        this.id = id;
        this.balance = 0.0;
        this.status = AccountStatus.ACTIVE;
        this.transactions = new ArrayList<>();
    }

    public boolean depositMoney(Double amount) {
        if(amount == null || amount <= 0) {
            throw new IllegalArgumentException(WRONG_ARGUMENT_PASSED);
        }
        this.balance = balance.doubleValue() + amount.doubleValue();
        Transaction deposit =  new Transaction(
                UUID.randomUUID(),
                TransactionType.DEPOSIT,
                amount,
                LocalDateTime.now());
        addTransaction(deposit);

        return true;
    }


    public boolean withdrawMoney(Double amount) {
        if(amount == null || amount <= 0) {
            throw new IllegalArgumentException(WRONG_ARGUMENT_PASSED);
        }
        if (!isWithdrawalAuthorized(amount)) {
            return false;
        }
        this.balance = balance.doubleValue() - amount.doubleValue();
        Transaction withdrawal =  new Transaction(
                UUID.randomUUID(),
                TransactionType.WITHDRAWAL,
                amount,
                LocalDateTime.now());
        addTransaction(withdrawal);

        return true;
    }


    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
    }


    public boolean isWithdrawalAuthorized(Double amount) {
        return this.balance > amount;
    }


    public boolean closeAccount() {
        if (AccountStatus.INACTIVE.equals(this.status)) {
            return false;
        }
        this.status = AccountStatus.INACTIVE;
        return true;
    }


    public boolean activateAccount() {
        if (AccountStatus.ACTIVE.equals(this.status)) {
            return false;
        }
        this.status = AccountStatus.ACTIVE;
        return true;
    }
}
