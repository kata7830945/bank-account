package fr.exaltit.medkhalilbankaccount.domains.transaction.enums;

public enum TransactionType {
    DEPOSIT("DEPOSIT"),
    WITHDRAWAL("WITHDRAWAL");
    public final String type;
    TransactionType(String type) {
        this.type = type;
    }
}
