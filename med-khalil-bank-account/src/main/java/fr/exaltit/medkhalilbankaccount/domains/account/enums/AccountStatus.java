package fr.exaltit.medkhalilbankaccount.domains.account.enums;

public enum AccountStatus {
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");
    public final String status;
    AccountStatus(String status) {
        this.status = status;
    }
}
