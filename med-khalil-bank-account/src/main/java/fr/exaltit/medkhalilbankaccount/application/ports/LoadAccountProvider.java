package fr.exaltit.medkhalilbankaccount.application.ports;

import fr.exaltit.medkhalilbankaccount.domains.account.Account;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LoadAccountProvider {

    List<Account> loadAll();

    Optional<Account> loadAccount(UUID id);
}
