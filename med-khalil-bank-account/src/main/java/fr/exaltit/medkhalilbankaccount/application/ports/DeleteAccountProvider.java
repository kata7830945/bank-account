package fr.exaltit.medkhalilbankaccount.application.ports;

import fr.exaltit.medkhalilbankaccount.domains.account.Account;

public interface DeleteAccountProvider {

    void delete(Account account);

}
