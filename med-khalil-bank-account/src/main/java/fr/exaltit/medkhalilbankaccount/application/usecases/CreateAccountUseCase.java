package fr.exaltit.medkhalilbankaccount.application.usecases;

import fr.exaltit.medkhalilbankaccount.domains.account.Account;

public interface CreateAccountUseCase {

    Account createAccount();
}
