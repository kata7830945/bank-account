package fr.exaltit.medkhalilbankaccount.application.usecases;

import fr.exaltit.medkhalilbankaccount.domains.account.Account;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;

import java.util.List;
import java.util.UUID;

public interface ConsultAccountUseCase {

    List<Account> consultAccount();

    Account consultAccount(UUID id);

    Double consultBalance(UUID id);

    List<Transaction> consultTransactions(UUID id);

}
