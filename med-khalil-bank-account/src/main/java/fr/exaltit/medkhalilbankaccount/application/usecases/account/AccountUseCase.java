package fr.exaltit.medkhalilbankaccount.application.usecases.account;

import fr.exaltit.medkhalilbankaccount.application.ports.DeleteAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.ports.LoadAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.ports.SaveAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.usecases.ConsultAccountUseCase;
import fr.exaltit.medkhalilbankaccount.application.usecases.CreateAccountUseCase;
import fr.exaltit.medkhalilbankaccount.application.usecases.ModifyAccountUseCase;
import fr.exaltit.medkhalilbankaccount.domains.account.Account;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Data
@AllArgsConstructor
public class AccountUseCase implements ConsultAccountUseCase,
        ModifyAccountUseCase, CreateAccountUseCase {

    private LoadAccountProvider loadAccountProvider;
    private SaveAccountProvider saveAccountProvider;
    private DeleteAccountProvider deleteAccountProvider;

    @Override
    public Account createAccount() {
        Account newAccount = new Account(UUID.randomUUID());

        saveAccountProvider.save(newAccount);
        return newAccount;
    }

    @Override
    public List<Account> consultAccount() {
        return loadAccountProvider.loadAll();
    }

    @Override
    public Account consultAccount(UUID accountId) {
        return loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public boolean depositAmount(UUID accountId, Double amount) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        boolean isDeposit = account.depositMoney(amount);
        if (isDeposit) {
            saveAccountProvider.save(account);
        }

        return isDeposit;
    }

    @Override
    public boolean withdrawAmount(UUID accountId, Double amount) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        boolean isWithdrawal = account.withdrawMoney(amount);
        if (isWithdrawal) {
            saveAccountProvider.save(account);
        }

        return isWithdrawal;
    }

    @Override
    public Double consultBalance(UUID accountId) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        return account.getBalance();
    }

    @Override
    public List<Transaction> consultTransactions(UUID accountId) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        return account.getTransactions();
    }

    @Override
    public boolean closeAccount(UUID accountId) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        boolean isClosedAccount = account.closeAccount();

        if (isClosedAccount) {
            saveAccountProvider.save(account);
        }
        return isClosedAccount;
    }

    @Override
    public boolean activate(UUID accountId) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        boolean isActiveAccount = account.activateAccount();
        if (isActiveAccount) {
            saveAccountProvider.save(account);
        }
        return isActiveAccount;
    }

    @Override
    public void deleteAccount(UUID accountId) {
        Account account = loadAccountProvider.loadAccount(accountId)
                .orElseThrow(NoSuchElementException::new);

        deleteAccountProvider.delete(account);
    }

}

