package fr.exaltit.medkhalilbankaccount.application.usecases;

import java.util.UUID;

public interface ModifyAccountUseCase {

    boolean activate(UUID id);

    boolean withdrawAmount(UUID id, Double amount);

    boolean depositAmount(UUID id, Double amount);

    boolean closeAccount(UUID id);

    void deleteAccount(UUID id);

    
}
