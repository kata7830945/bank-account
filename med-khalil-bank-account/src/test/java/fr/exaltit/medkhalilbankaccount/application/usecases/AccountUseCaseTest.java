package fr.exaltit.medkhalilbankaccount.application.usecases;

import fr.exaltit.medkhalilbankaccount.application.ports.DeleteAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.ports.LoadAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.ports.SaveAccountProvider;
import fr.exaltit.medkhalilbankaccount.application.usecases.account.AccountUseCase;
import fr.exaltit.medkhalilbankaccount.domains.account.Account;
import fr.exaltit.medkhalilbankaccount.domains.account.enums.AccountStatus;
import fr.exaltit.medkhalilbankaccount.domains.account.enums.AccountStatusTest;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AccountUseCaseTest {
    public static final String ACTIVE = "ACTIVE";
    public static final String INACTIVE = "INACTIVE";
    private static final Double SMALL_AMOUNT = Double.valueOf(100.00);
    private static final Double BIG_AMOUNT = Double.valueOf(5000.00);
    private static final Double ACCOUNT_BALANCE = Double.valueOf(3000.00);

    private LoadAccountProvider loadAccountProvider;
    private SaveAccountProvider saveAccountProvider;
    private DeleteAccountProvider deleteAccountProvider;
    private AccountUseCase accountUseCase;
    private static UUID sharedUUID;
    private static Account sharedAccount;

    @BeforeAll
    public static void initAccount() {
        sharedUUID = UUID.randomUUID();
        sharedAccount = new Account(sharedUUID, ACCOUNT_BALANCE);
    }

    @BeforeEach
    void init() {
        loadAccountProvider = mock(LoadAccountProvider.class);
        when(loadAccountProvider.loadAccount(sharedUUID)).thenReturn(Optional.of(sharedAccount));

        saveAccountProvider = mock(SaveAccountProvider.class);
        deleteAccountProvider = mock(DeleteAccountProvider.class);

        accountUseCase = new AccountUseCase(loadAccountProvider, saveAccountProvider, deleteAccountProvider);
    }

    @Test
    void shouldCreateAccount_WhenIdNotNull() {
        Account account = accountUseCase.createAccount();
        verify(saveAccountProvider, times(1)).save(any(Account.class));
        assertNotNull(account.getId());
    }

    @Test
    void shouldIncreaseBalance_whenDepositMoney() {
        boolean isIncreasedBalance = accountUseCase.depositAmount(sharedUUID, SMALL_AMOUNT);
        verify(saveAccountProvider, times(1)).save(any(Account.class));
        assertEquals(sharedAccount.getId(), sharedUUID);
        assertTrue(isIncreasedBalance);
    }

    @Test
    void shouldThrowException_WhenDepositMoneyWithNoAccountFound() {
        UUID randomId = UUID.randomUUID();
        assertThrows(NoSuchElementException.class,
                () -> {
                    accountUseCase.depositAmount(randomId, BIG_AMOUNT);
                });
        assertNotEquals(UUID.randomUUID(), sharedAccount.getId());
    }

    @Test
    void shouldDecreaseBalance_WhenWithdrawMoney() {
        boolean isDecreasedBalance = accountUseCase.withdrawAmount(sharedUUID, SMALL_AMOUNT);
        verify(saveAccountProvider, times(1)).save(any(Account.class));
        assertEquals(sharedAccount.getId(), sharedUUID);
        assertTrue(isDecreasedBalance);
    }

    @Test
    void shouldNotDecreaseBalance_WhenNoAmountAvailable() {
        boolean isDecreasedBalance = accountUseCase.withdrawAmount(sharedUUID, BIG_AMOUNT);
        verify(saveAccountProvider, times(0)).save(any(Account.class));
        assertEquals(sharedAccount.getId(), sharedUUID);
        assertFalse(isDecreasedBalance);
    }

    @Test
    void shouldThrowException_WhenWithdrawMoneyWithNoAccountFound() {
        UUID randomId = UUID.randomUUID();
        assertThrows(NoSuchElementException.class,
                () -> {
                    accountUseCase.withdrawAmount(randomId, SMALL_AMOUNT);
                });
        assertEquals(sharedUUID, sharedAccount.getId());
    }

    @Test
    void shouldReturnAccountBalance_WhenConsultBalance() {
        Double returnedBalance = accountUseCase.consultBalance(sharedUUID);
        assertNotNull(returnedBalance);
    }

    @Test
    void shouldThrowException_WhenConsultBalance() {
        UUID randomId = UUID.randomUUID();
        assertThrows(NoSuchElementException.class,
                () -> {
                    accountUseCase.consultBalance(randomId);
                });
    }

    @Test
    void shouldReturnTransactions_WhenConsultTransactions() {
        List<Transaction> transactions = accountUseCase.consultTransactions(sharedUUID);
        assertNotNull(transactions);
    }

    @Test
    void shouldThrowException_WhenConsultTransactions() {
        UUID randomId = UUID.randomUUID();
        assertThrows(NoSuchElementException.class,
                () -> {
                    accountUseCase.consultTransactions(randomId);
                });
    }

    @Test
    void shouldCloseAccount_WhenAccountIsActive() {
        if (!AccountStatusTest.INACTIVE.equals(sharedAccount.getStatus())) {
            boolean isClosedAccount = accountUseCase.closeAccount(sharedUUID);
            verify(saveAccountProvider, times(1)).save(any(Account.class));
            assertEquals(sharedAccount.getStatus().status, INACTIVE);
            assertTrue(isClosedAccount);
        }
    }

    @Test
    void shouldNotCloseAccount_WhenAccountIsInactive() {
        if (AccountStatusTest.INACTIVE.equals(sharedAccount.getStatus())) {
            boolean isClosedAccount = accountUseCase.closeAccount(sharedUUID);
            verify(saveAccountProvider, times(0)).save(any(Account.class));
            assertEquals(sharedAccount.getStatus().status, INACTIVE);
            assertFalse(isClosedAccount);
        }
    }

    @Test
    void shouldActivateAccount_WhenAccountIsInactive() {
        if (!AccountStatusTest.ACTIVE.equals(sharedAccount.getStatus())) {
            boolean isActivatedAccount = accountUseCase.activate(sharedUUID);
            verify(saveAccountProvider, times(1)).save(any(Account.class));
            assertEquals(sharedAccount.getStatus().status, ACTIVE);
            assertTrue(isActivatedAccount);
        }
    }

    @Test
    void shouldNotActivateAccount_WhenAccountIsActive() {
        if (AccountStatusTest.ACTIVE.equals(sharedAccount.getStatus())) {
            boolean isActivatedAccount = accountUseCase.activate(sharedUUID);
            verify(saveAccountProvider, times(0)).save(any(Account.class));
            assertEquals(sharedAccount.getStatus().status, ACTIVE);
            assertFalse(isActivatedAccount);
        }
    }

    @Test
    void shouldThrowException_WhenNoAccountFound() {
        AccountStatus accountStatus = sharedAccount.getStatus();
        UUID randomId = UUID.randomUUID();
        assertThrows(NoSuchElementException.class,
                () -> {
                    accountUseCase.activate(randomId);
                });
        assertEquals(accountStatus, sharedAccount.getStatus());
    }

    @Test
    void shouldThrowException_WhenAccountIsDeleted() {
        UUID randomId = UUID.randomUUID();
        assertThrows(NoSuchElementException.class,
                () -> {
                    accountUseCase.deleteAccount(randomId);
                });
    }

}