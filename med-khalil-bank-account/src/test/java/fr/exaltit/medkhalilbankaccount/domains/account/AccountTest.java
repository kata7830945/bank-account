package fr.exaltit.medkhalilbankaccount.domains.account;

import fr.exaltit.medkhalilbankaccount.domains.account.enums.AccountStatus;
import fr.exaltit.medkhalilbankaccount.domains.transaction.Transaction;
import fr.exaltit.medkhalilbankaccount.domains.transaction.enums.TransactionType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

class AccountTest {
    private static final Double SMALL_AMOUNT = Double.valueOf(10.00);
    private static final Double BIG_AMOUNT = Double.valueOf(70.00);
    private static final Double NEGATIVE_AMOUNT = Double.valueOf(-30.00);
    private static final Double ZERO_AMOUNT = Double.valueOf(00.00);
    private static final Double NULL_AMOUNT = null;


    private static Transaction aTransactionDepositType() {
        return new Transaction(UUID.randomUUID(), TransactionType.DEPOSIT, Double.valueOf(20.00), LocalDateTime.now());
    }

    private static Transaction aTransactionWithdrawalType() {
        return new Transaction(UUID.randomUUID(), TransactionType.WITHDRAWAL, Double.valueOf(20.00), LocalDateTime.now());
    }

    private static Account aAccountDepositTransactionType() {
        return new Account(UUID.randomUUID(), Double.valueOf(50.00), Collections.singletonList(aTransactionDepositType()));
    }

    private static Account aAccountWithdrawalTransactionType() {
        return new Account(UUID.randomUUID(), Double.valueOf(50.00), Collections.singletonList(aTransactionWithdrawalType()));
    }

    private static Account aAccount() {
        return new Account(UUID.randomUUID(), Double.valueOf(50.00));
    }

    public static Stream<Arguments> correctDepositAmountProvider() {
        return Stream.of(
                Arguments.of(aAccount(), BIG_AMOUNT),
                Arguments.of(aAccount(), SMALL_AMOUNT)
        );
    }

    public static Stream<Arguments> correctWithdrawalAmountProvider() {
        return Stream.of(
                Arguments.of(aAccount(), SMALL_AMOUNT)
        );
    }

    public static Stream<Arguments> wrongDepositAndWithdrawalAmountProvider() {
        return Stream.of(
                Arguments.of(aAccount(), NEGATIVE_AMOUNT),
                Arguments.of(aAccount(), NULL_AMOUNT),
                Arguments.of(aAccount(), ZERO_AMOUNT)
        );
    }

    public static Stream<Arguments> correctTransactionTypeProvider() {
        return Stream.of(
                Arguments.of(aAccountDepositTransactionType(), TransactionType.DEPOSIT),
                Arguments.of(aAccountWithdrawalTransactionType(), TransactionType.WITHDRAWAL)
        );
    }

    public static Stream<Arguments> isWithdrawalAuthorizedCorrectProvider() {
        return Stream.of(
                Arguments.of(aAccount(), SMALL_AMOUNT, true),
                Arguments.of(aAccount(), BIG_AMOUNT, false)
        );
    }


    @ParameterizedTest
    @MethodSource("correctDepositAmountProvider")
    void shouldIncreaseBalance_WhenDepositMoney(Account account, Double amount) {
        Double balanceBefore = account.getBalance();

        account.depositMoney(amount);

        assertThat(account.getBalance()).isGreaterThanOrEqualTo(balanceBefore);
    }

    @ParameterizedTest
    @MethodSource("wrongDepositAndWithdrawalAmountProvider")
    void shouldThrowIllegalArgumentException_WhenDepositMoneyWithNegativeOrNull(Account account, Double amount) {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            account.depositMoney(amount);
        });
    }

    @ParameterizedTest
    @MethodSource("correctDepositAmountProvider")
    void shouldReturnCorrectBalance_WhenDepositMoney(Account account, Double amount) {
        Double balanceBefore = account.getBalance();

        account.depositMoney(amount);

        Double returnedBalance = account.getBalance();
        Double expectedBalance = balanceBefore + amount;

        assertThat(expectedBalance).isEqualTo(returnedBalance);
    }

    @ParameterizedTest
    @MethodSource("correctWithdrawalAmountProvider")
    void shouldDecreaseBalance_WhenWithdrawMoney(Account account, Double amount) {
        Double balanceBefore = account.getBalance();

        account.withdrawMoney(amount);

        assertThat(account.getBalance()).isLessThanOrEqualTo(balanceBefore);
    }

    @ParameterizedTest
    @MethodSource("wrongDepositAndWithdrawalAmountProvider")
    void shouldThrowIllegalArgumentException_WhenWithdrawalMoneyWithNegativeOrNull(Account account, Double amount) {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            account.withdrawMoney(amount);
        });
    }

    @ParameterizedTest
    @MethodSource("correctWithdrawalAmountProvider")
    void shouldReturnCorrectBalance_WhenWithdrawMoney(Account account, Double amount) {
        Double balanceBefore = account.getBalance();

        account.withdrawMoney(amount);

        Double returnedBalance = account.getBalance();
        Double expectedBalance = Double.valueOf(balanceBefore - amount);

        assertThat(expectedBalance).isEqualTo(returnedBalance);
    }

    @ParameterizedTest
    @MethodSource("correctDepositAmountProvider")
    void shouldAddTransactionWithCorrectAmount_WhenDepositMoney(Account account, Double amount) {
        Double expectedBalance = amount;

        account.depositMoney(amount);

        Double returnedBalance = account.getTransactions().get(0).getAmount();

        assertThat(expectedBalance).isEqualTo(returnedBalance);
    }

    @ParameterizedTest
    @MethodSource("correctWithdrawalAmountProvider")
    void shouldAddTransactionWithCorrectAmount_WhenWithdrawMoney(Account account, Double amount) {
        Double expectedBalance = amount;

        account.withdrawMoney(amount);

        Double returnedBalance = account.getTransactions().get(0).getAmount();

        assertThat(expectedBalance).isEqualTo(returnedBalance);
    }

    @Test
    void shouldAddTransactionWithCorrectType_WhenDepositMoney() {
        Account account = aAccount();
        TransactionType expectedTransactionType = TransactionType.DEPOSIT;

        account.depositMoney(SMALL_AMOUNT);

        TransactionType returnedTransactionType = account.getTransactions().get(0).getType();

        assertThat(expectedTransactionType).isEqualTo(returnedTransactionType);

    }

    @Test
    void shouldAddTransactionWithCorrectType_WhenWithdrawMoney() {
        Account account = aAccount();
        TransactionType expectedTransactionType = TransactionType.WITHDRAWAL;

        account.withdrawMoney(SMALL_AMOUNT);

        TransactionType returnedTransactionType = account.getTransactions().get(0).getType();

        assertThat(expectedTransactionType).isEqualTo(returnedTransactionType);
    }

    @ParameterizedTest
    @MethodSource("isWithdrawalAuthorizedCorrectProvider")
    void shouldReturnCorrectBoolean_WhenWithdrawalIsOrNotAuthorized(Account account, Double amount, Boolean isAuthorized) {
        assertThat(account.isWithdrawalAuthorized(amount)).isEqualTo(isAuthorized);
    }

    @Test
    void shouldRefuseWithdrawal_WhenWithdrawMoneyWithInsufficientBalance() {
        assertThat(aAccount().withdrawMoney(BIG_AMOUNT)).isFalse();
    }

    @Test
    void shouldAddTransaction_WhenAddingTransactionOfDepositType() {
        Transaction transaction = new Transaction(
                UUID.randomUUID(), TransactionType.DEPOSIT, BIG_AMOUNT, LocalDateTime.now());

        Account account = aAccount();
        account.addTransaction(transaction);

        assertThat(account.getTransactions().contains(transaction));
    }

    @Test
    void shouldAddTransaction_WhenAddingTransactionOfWithdrawalType() {
        Transaction transaction = new Transaction(
                UUID.randomUUID(), TransactionType.WITHDRAWAL, BIG_AMOUNT, LocalDateTime.now());

        Account account = aAccount();
        account.addTransaction(transaction);

        assertThat(account.getTransactions()).contains(transaction);
    }

    @Test
    void shouldReturnActiveStatus_WhenAccountIsActivated() {
        AccountStatus expectedAccountStatus = AccountStatus.ACTIVE;

        Account account = aAccount();
        account.closeAccount();
        account.activateAccount();

        AccountStatus returnedAccountStatus = account.getStatus();
        assertThat(expectedAccountStatus).isEqualTo(returnedAccountStatus);
    }

    @Test
    void shouldReturnInactiveStatus_WhenAccountIsClosed() {
        AccountStatus expectedAccountStatus = AccountStatus.INACTIVE;

        Account account = aAccount();
        account.closeAccount();
        AccountStatus returnedAccountStatus = account.getStatus();

        assertThat(expectedAccountStatus).isEqualTo(returnedAccountStatus);
    }

    @Test
    void shouldReturnFalse_WhenAccountIsAlreadyClosed() {
        Account account = aAccount();
        account.closeAccount();
        assertThat(account.closeAccount()).isFalse();
    }

    @Test
    void shouldReturnFalse_WhenAccountIsAlreadyActivated() {
        assertThat(aAccount().activateAccount()).isFalse();
    }


}